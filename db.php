<?php

$servername = "localhost"; // Der Hostname des Servers, auf dem sich die Datenbank befindet
$username = "root"; // Ihr Datenbank-Benutzername
$password = ""; // Ihr Datenbank-Passwort
$dbname = "test_db"; // Der Name Ihrer Datenbank

// Eine Verbindung zur Datenbank herstellen
$conn = new mysqli($servername, $username, $password, $dbname);

// Die Verbindung überprüfen
if ($conn->connect_error) {
    error_log("Verbindung fehlgeschlagen: " . $conn->connect_error);
}
error_log("Verbindung erfolgreich");

// SQL-Abfrage, um die benötigten Namen zu holen
$sql = "SELECT select_name FROM all_links"; // Ersetzen Sie 'your_table' durch den Namen Ihrer Tabelle

$result = $conn->query($sql);

$conn->close();

?>